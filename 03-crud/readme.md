# Application Skeleton

# OUR FIRST CRUD

We have a base to work from, now, let's build our first Model.

We call "Model" any type of repeatable data following the same principles.

We already created a model, "Contacts". It has a table, and even some data.

We are going to:

1. create methods in the controller to `Create`, `Read`, `Add`, `Update`, and `Delete` contacts
2. create routes in Express to access each of those
3. set some way to control those routes from React

Let's start

1. [Add CRUD to the controller](./01-controller)
2. [Add CRUD to the HTTP Router](./02-routes)
3. [Make React use the CRUD](./03-react)
4. [Add Interaction Feedback for React](./04-interaction)
5. [Add Very Simple Authentication](./05-auth)
6. [RECAP](./99-recap)

