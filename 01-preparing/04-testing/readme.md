## Testing

Of course, we need to set up some unit testing. We will not create tests right now, but we will setup the environment so tests are possible.

For the front-end, this is all set for us by `create-react-app`. You can check that, just go to the `front` directory, run 

```sh
npm test
```

And you will see the result right away. You will get an error due to `react-native-web` needing canvas.

We'll fix it by providing a false function to the tests. create a file called `setupTests.js` in  `front/src`, and write in it:

```js
// front/src/setupTests.js
HTMLCanvasElement.prototype.getContext = () => ({})
```

This creates a function that returns nothing, replacing the native function needed. It is used for testing only.

**note**: `setupTests.js` is used by the test suite of `create-react-app`. It is not a generic way of setting tests up. It's *only* for `create-react-app`.

run the tests again, and they should pass without warnings.

Now, let's take care of the back-end. There are many testing libraries to choose from, but `create-react-app` have decided on `jest`. For consistency, we will use that on the back-end too. Move to the `back` directory.

```sh
npm install --save-dev babel-jest babel-core@^7.0.0-bridge.0 regenerator-runtime
```

It would be nice if we could just install jest, unfortunately, it doesn't really work (see the issue [here](https://github.com/facebook/jest/issues/6913)), so we have to install that bridge thing.

add the script to `package.json`:

```json
{ ...
  "scripts":{
    ...
    "test":"jest"
  }
}
```

let's create two files, just to make sure things work (we can remove them later)

```js
// src/sum.js
export default (a, b) => a+b
```

```js
// src/sum.test.js
import sum from  './sum'

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});
```

run

```sh
npm test
```

you can also try:

```sh
npm test -- --coverage
```

For coverage report.

You can delete those two files now, we know testing works.
