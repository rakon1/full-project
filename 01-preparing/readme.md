# THE PREPARING

We are going to build an application with front-end and back-end, step by step

1. [Create front-end](./01-create-react-app)
2. [Create the backend](./02-backend)
3. [Linking Front and Back](./03-front-and-back)
4. [Set up testing]('./04-testing)
5. Optionally, [Set up Visual Studio Code](./05-vscode)
8. [RECAP](./99-recap)